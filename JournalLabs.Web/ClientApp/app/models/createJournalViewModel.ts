import { LabBlock } from "./LabBlock"
export class CreateJournalViewModel {
  LessonName: string = "";
  StudentsCount: number = 0;
  LabBlocksSettings: LabBlock[] = [];
  TeacherIds: string[] = [];
}
